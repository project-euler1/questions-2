#By considering the terms in the Fibonacci sequence whose values do not exceed four million, find the sum of the even-valued terms.

placeholder = 1
firstValue = 1
secondValue = 2
sum = 0
while(secondValue <=4000000):
    sum+= secondValue if secondValue%2 == 0 else 0
    placeholder = secondValue
    secondValue += firstValue
    firstValue = placeholder
print(sum)

#Answer: 4613732